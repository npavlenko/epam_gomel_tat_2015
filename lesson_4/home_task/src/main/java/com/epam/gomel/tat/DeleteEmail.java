package com.epam.gomel.tat;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class DeleteEmail {

    // AUT data
    public static final String BASE_URL = "http://www.ya.ru";

    // UI data
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final By MAIL_SUBJECT_LOCATOR = By.xpath("//div[@class='b-messages b-messages_threaded']/div[1]//span[@class='b-messages__subject']");
    public static final By MAIL_CHECKBOX_LOCATOR = By.xpath("//label[@class='b-messages__message__checkbox']/input[1]");
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//a[@data-action='delete']");
    public static final By DELETE_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    public static final By SUBJECT_DELETED_MAIL_LOCATOR = By.xpath("//div[@class='b-messages']/div[1]//span[@class='b-messages__subject'][1]");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[.//*[text()='%s']]";

    // Tools data
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 20;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
    private WebDriver driver;

    // Test data
    private String userLogin = "pavlenko.natallia"; // ACCOUNT
    private String userPassword = "password1"; // ACCOUNT
    private String mailTo = "pavlenko.natallia@yandex.ru"; // ENUM
    private String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    private String mailContent = "mail content" + Math.random() * 100000000;// RANDOM

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @Test(description = "Success send mail without attachment")
    public void sendEmail() {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
    }

    @Test(description = "Delete email", dependsOnMethods = {"sendEmail"})
    public void deleteEmail(){

        WebElement inboxLink = driver.findElement(INBOX_LINK_LOCATOR);
        inboxLink.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject))));

        String mailSubject = driver.findElement(MAIL_SUBJECT_LOCATOR).getText();
        WebElement mailCheckbox = driver.findElement(MAIL_CHECKBOX_LOCATOR);
        mailCheckbox.click();
        WebElement deleteButton = driver.findElement(DELETE_BUTTON_LOCATOR);
        deleteButton.click();
        WebElement deleteLink = driver.findElement(DELETE_LINK_LOCATOR);
        deleteLink.click();

        driver.findElement(SUBJECT_DELETED_MAIL_LOCATOR).equals(mailSubject);
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }

    private WebElement waitForElement(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }
}
