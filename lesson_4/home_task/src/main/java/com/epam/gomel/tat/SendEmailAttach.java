package com.epam.gomel.tat;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class SendEmailAttach {
    // AUT data
    public static final String BASE_URL = "http://www.ya.ru";

    // UI data
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By ATTACH_FILES_BUTTON_LOCATOR = By.xpath("//input[@name='att']");
    public static final By OUTBOX_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final By EXPAND_SENDED_MAIL_LOCATOR = By.xpath("//div[@class='block-messages-list-box b-layout__first-pane']/div[2]//div[@class='b-messages b-messages_threaded']/div[1]//span[@class='b-messages__subject'][1]");
    public static final By SUBJECT_SENDED_MAIL_LOCATOR = By.xpath("//div[@class='block-thread']/div[1]//span[@class='b-messages__firstline']");
    public static final By DOWNLOAD_ATTACHMENT_LINK_LOCATOR = By.xpath("//div[@class='b-message-attachments_head']//span[@class='b-file__actions']/a[2]");
    public static final String PATH_TO_ATTACHMENT = "\\attach.txt";

    // Tools data
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
    private WebDriver driver;

    // Test data
    private String userLogin = "pavlenko.natallia"; // ACCOUNT
    private String userPassword = "password1"; // ACCOUNT
    private String mailTo = "pavlenko.natallia@yandex.ru"; // ENUM
    private String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    private String mailContent = "mail content" + Math.random() * 100000000;// RANDOM

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {
        FirefoxProfile fxProfile = new FirefoxProfile();

        fxProfile.setPreference("browser.download.folderList", 2);
        fxProfile.setPreference("browser.download.manager.showWhenStarting", false);
        fxProfile.setPreference("browser.download.dir","user.dir");
        fxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk","text/plain");

        driver = new FirefoxDriver(fxProfile);
        //driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @Test(description = "Success send mail with attachment")
    public void sendEmailAttach() {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);
        String projectPath = System.getProperty("user.dir");
        mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);

        mailContentText.sendKeys(projectPath+mailContent);
        WebElement attachFilesButton = driver.findElement(ATTACH_FILES_BUTTON_LOCATOR);

        attachFilesButton.sendKeys(projectPath+PATH_TO_ATTACHMENT);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        WebElement outboxLink = waitForElement(OUTBOX_LINK_LOCATOR);
        outboxLink.click();
        WebElement expandSignSendedMail = waitForElement(EXPAND_SENDED_MAIL_LOCATOR);
        expandSignSendedMail.click();
        WebElement subjectSendedMail = driver.findElement(SUBJECT_SENDED_MAIL_LOCATOR);
        subjectSendedMail.click();
        WebElement downloadAttachmentLink = driver.findElement(DOWNLOAD_ATTACHMENT_LINK_LOCATOR);
        downloadAttachmentLink.click();


        try {
            FileReader uploadedFile = new FileReader(PATH_TO_ATTACHMENT);
            FileReader downloadedFile = new FileReader("\\"+PATH_TO_ATTACHMENT);
            BufferedReader txtReader = new BufferedReader(uploadedFile);
            BufferedReader txtReader2 = new BufferedReader(downloadedFile);
            try {
                Assert.assertTrue(txtReader.readLine().equals(txtReader2.readLine()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }

    private WebElement waitForElement(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

}
